package Task3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Task3Test {
    @Test
    void task3() throws InterruptedException {
        WebDriver webDriver= new ChromeDriver();
        try {
            webDriver.get("https://mail.ru/");
            WebElement email = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            email.sendKeys("jin.mori.ibatech");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();
            Thread.sleep(1000);
            WebElement passwordLink = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y >" +
                    " div.password-input-container.svelte-1eyrl7y > input"));
            passwordLink.sendKeys("jinmori12345678");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y")).click();
            Thread.sleep(6000);
            String expectedResult= "Неверное имя или пароль";
            Assertions.assertEquals(expectedResult,webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[3]")).getText());

        } finally {
            webDriver.close();
        }


    }
}
