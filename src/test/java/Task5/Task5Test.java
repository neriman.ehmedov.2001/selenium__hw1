package Task5;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class Task5Test {
    @Test
    void task5() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://www.etsy.com/");
            Actions builder = new Actions(webDriver);
            WebElement mainmenu1 = webDriver.findElement(By.xpath("//*[@id=\"catnav-primary-link-10923\"]"));
            builder.moveToElement(mainmenu1).build().perform();
            Thread.sleep(1000);
            WebElement submenu1 = webDriver.findElement(By.xpath("//*[@id=\"side-nav-category-link-10936\"]"));
            builder.moveToElement(submenu1).click().build().perform();
            Thread.sleep(1000);
            webDriver.findElement(By.xpath("//*[@id=\"catnav-l4-11109\"]")).click();
            Thread.sleep(1000);
             WebElement link= webDriver.findElement(By.cssSelector("#content > div > div.wt-bg-white." +
                "wt-grid__item-md-12.wt-pl-xs-1.wt-pr-xs-0.wt-pr-md-1.wt-pl-lg-0.wt-pr-lg-0.wt-bb-xs-1 >" +
                " div > div.wt-mt-xs-2.wt-pl-xs-1.wt-pl-md-4.wt-pl-lg-6.wt-pr-xs-1.wt-pr-md-4.wt-pr-lg-6 > " +
                "div > span > span > span:nth-child(2)"));
        System.out.println(link.getText());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }


    }
}
