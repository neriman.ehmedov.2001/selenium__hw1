package Task4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Task4test {
    @Test
    void  task4() throws InterruptedException {
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://mail.ru/");
            WebElement email = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            email.sendKeys("jin.mori.ibatech");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y")).click();

            WebElement passwordLink = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y >" +
                    " div.password-input-container.svelte-1eyrl7y > input"));
            passwordLink.sendKeys("jinmori1234");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button." +
                    "svelte-1eyrl7y")).click();
            Thread.sleep(9000);

            WebElement messageLink = webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > " +
                    "div.application-mail__overlay > div > div.application-mail__layout.application-mail__layout_main" +
                    " > span > div.layout__column.layout__column_left > div.layout__column-wrapper > div > div > div " +
                    "> div:nth-child(1) > div > div > a > span"));
            messageLink.click();
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement newmail = webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[1]/div[2]/div[3]/div[2]" +
                    "/div/div/div[1]/div/div[2]/div/div/label/div/div/input"));
            newmail.sendKeys("jin.mori.ibatech@mail.ru");

            webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[1]" +
                    "/div[2]/div[3]/div[3]/div[1]/div[2]/div/input")).sendKeys("Iba_hw1");
            webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[1]/div[2]/div[3]" +
                    "/div[5]/div/div/div[2]/div[1]/div[1]")).sendKeys("Hi, attaching file is " +
                    "not working in here , all of my classmates have looked at this and even " +
                    "teacher looked but it was not working");
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement sendingButton = webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[2]/div[1]/span[1]/span/span"));
            sendingButton.click();
            String  expectedResult="https://e.mail.ru/inbox/?back=1&afterReload=1";
            Assertions.assertEquals(expectedResult,webDriver.getCurrentUrl());
        } finally {
            webDriver.quit();
        }


    }
}
