package Task2;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task2 {
    @Test
    void task2(){
        String expectedUrl = "https://e.mail.ru/inbox/?back=1&afterReload=1";
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://mail.ru/");
            WebElement email = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            email.sendKeys("jin.mori.ibatech");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y >" +
                    " button.button.svelte-1eyrl7y")).click();

            WebElement passwordLink = webDriver.findElement(By.cssSelector("#mailbox >" +
                    " form.body.svelte-1eyrl7y >" +
                    " div.password-input-container.svelte-1eyrl7y > input"));
            passwordLink.sendKeys("jinmori1234");
            webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y >" +
                    " button.second-button.svelte-1eyrl7y")).click();

            assertEquals(expectedUrl, webDriver.getCurrentUrl());

        } finally {
            webDriver.close();
        }

    }
}
