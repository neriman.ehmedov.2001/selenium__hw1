package Task1;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task1Test {
    @Test
    void task1(){
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://google.com");
            WebElement searching = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > " +
                    "form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));
            searching.sendKeys("Selenium");
            webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf >" +
                    " form > div:nth-child(1) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b")).click();
            webDriver.findElement(By.cssSelector(" #rso > div:nth-child(1) > div > div > div.yuRUbf > a > h3")).click();
            //wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("")))
            //Thread.sleep(100);
            String expectedResult = "Selenium automates browsers. That's it!";
            assertEquals(expectedResult, webDriver.findElement(By.cssSelector("body > section.hero.homepage > h1:nth-child(1)")).getText());
        } finally {
            webDriver.close();
        }

    }
}
